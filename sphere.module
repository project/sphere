<?php
// $$
/**
 * @file
 * Adds "related content" links to nodes via sphere.com.
 */

/**
 * Implementation of hook_perm().
 */
function sphere_perm() {
  return array("administer sphere");
}

/**
 * Implementation of hook_menu().
 */
function sphere_menu() {
  $items = array();
  $items['admin/settings/sphere'] = array(
    'title' => "Sphere",
    'description' => "Configure integration with Sphere.com",
    'access arguments' => array("administer sphere"),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sphere_settings_form'),
    );

  return $items;
}

/**
 * Implementation of hook_link().
 */
function sphere_link($type, $object = NULL, $teaser = FALSE) {
  static $added_js;
  $links = array();
  $types = variable_get("sphere_node_types", array());
  if ($type == 'node' && in_array($object->type, array_values($types), TRUE)) {
    drupal_add_css(drupal_get_path('module', 'sphere') .'/sphere.css');
    $permalink = url(drupal_get_path_alias('node/'.$object->nid), array('absolute' => TRUE));
    if (!isset($added_js)) {
      drupal_set_html_head('<script type="text/javascript" src="http://www.sphere.com/widgets/sphereit/js?t=classic&amp;p='. url('', array('absolute' => TRUE)) .'"></script>');
      $added_js = TRUE;
    }
    if ($teaser) {
      $links['sphere_related_content'] = array(
        'title' => t("Sphere: Related Content"),
        'href' => "http://www.sphere.com/search?q=sphereit:". $permalink,
        'attributes' => array(
          'class' => 'iconsphere',
          'title' => t("Related Blogs & Articles"),
          'onclick' => 'return Sphere.Widget.search("'. $permalink .'")',
          ),
        );
    }
    else {
      $links['sphere_related_content'] = array(
        'title' => t("Sphere: Related Content"),
        'href' => "http://www.sphere.com/search?q=sphereit:". $permalink,
        'attributes' => array(
          'class' => 'iconsphere',
          'title' => t("Related Blogs & Articles"),
          'onclick' => "return Sphere.Widget.search()",
          ),
        );
    }
  }
  return $links;
}

/**
 * Implementation of hook_nodeapi().
 */
function sphere_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    // we use the view op rather than alter because we need the $page parameter.
    case 'view':
      $types = variable_get("sphere_node_types", array());
      if (in_array($node->type, array_values($types), TRUE) && $a4) {
        // add comments that help Sphere index content.
        $node->content['sphere_preamble'] = array(
          '#value' => '<!-- sphereit start -->',
          '#weight' => -10,
          );
        $node->content['sphere_postamble'] = array(
          '#value' => '<!-- sphereit end -->',
          '#weight' => 10,
          );
      }
      break;
  }
}

/**
 * Settings form.
 */
function sphere_settings_form() {
  $form = array();
  $form['sphere_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#description' => t("The types of nodes that should display the Sphere related content link."),
    '#default_value' => variable_get('sphere_node_types', array()),
    '#options' => node_get_types('names')
    );
  return system_settings_form($form);
}